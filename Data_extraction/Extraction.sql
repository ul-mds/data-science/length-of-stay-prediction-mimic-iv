
-- for the creation of the readmission table; first drop if existing; 
drop materialized view if exists no_readmissions cascade;
-- get all the stay_id from all ICU patients who return not in 48 hours (2 days)
create materialized view no_readmissions as
  with readmission_data as (
			select time_data.stay_id, (prev_dd > intime - interval '2 day')::int as num_readmissions
				from (select i.*,
             lag(i.outtime) over (partition by i.hadm_id) as prev_dd
      from icustays i) as time_data
) select stay_id from readmission_data
where num_readmissions is null or num_readmissions <= 0;

-- chart data withing the first 24 hours
-- metric used: mean
drop materialized view if exists chart_data cascade;
create materialized view chart_data as
select * from crosstab(
	'with group_chart_data as (
		with chart_data as(
			select c.subject_id , c.hadm_id , c.stay_id , c.itemid , c.value, c.valuenum , c.charttime , i.intime , (c.charttime- i.intime) as timediff 
			from chartevents c
				inner join icustays i 
					on i.stay_id = c.stay_id 
			) 
		select subject_id, hadm_id, stay_id, itemid, value,valuenum , charttime, intime, timediff 
		from chart_data
			where timediff > ''00:00:00'' and timediff <= ''24:00:00''
			and value is not null 
			and itemid in (''220739'',''223901'',''223900'', ''220045'', ''223761'', ''220277'', ''220210'') -- select the item ids
	) 	
	select stay_id, itemid, avg(valuenum) -- mean function from postgresql
	from group_chart_data
	group by stay_id, itemid'
	) as ct(stay_id integer, Heart_rate double precision, Respiration double precision, Saturation double precision, GLC_eye double precision,
			Temperature double precision , GLC_verbal double precision, GLC_motor double precision);

-- laboratory data withing the first 24 hours
-- metric used: mean
drop materialized view if exists lab_data cascade;
create materialized view lab_data as
select * from crosstab(
	'with group_lab_data as(
		with lab_data as(
			select l.subject_id , l.hadm_id, i.stay_id, dli.label as itemlabel,l.itemid  , l.value, l.valuenum , l.charttime , i.intime , (l.charttime- i.intime) as timediff 
			from labevents l
				inner join icustays i 
					on i.hadm_id = l.hadm_id
				inner join d_labitems dli
					on dli.itemid = l.itemid
			) 
		select subject_id,stay_id, hadm_id,itemlabel,itemid, value,valuenum , charttime, intime, timediff
		from lab_data
			where timediff > ''00:00:00'' and timediff <= ''24:00:00''
			and value is not null 
			and itemid in (''50868'',''50882'',''50902'', ''50912'', ''50931'', ''50960'', ''50971'', ''50983'', ''51006'', ''51221'', ''51222'', 
						   ''51248'',''51249'',''51250'',''51265'',''51277'',''51279'',''51301'')
	) 	
	select stay_id, itemlabel, avg(valuenum)
	from group_lab_data
	group by stay_id, itemlabel
	order by 1',
	$$Values ('Anion Gap'), ('Bicarbonate'), ('Chloride'), ('Creatinine'), ('Glucose'),
	('Magnesium'), ('Potassium'), ('Sodium'), ('Urea Nitrogen'), ('Hematocrit'),
	('Hemoglobin'), ('MCH'), ('MCHC'), ('MCV'), ('Platelet Count'), ('RDW'),
	('Red Blood Cells'), ('White Blood Cells')$$) 
	as ct(stay_id integer, "Anion Gap" double precision, "Bicarbonate" double precision, "Chloride" double precision, "Creatinine" double precision,
			"Glucose" double precision , "Magnesium" double precision, "Potassium" double precision, "Sodium" double precision, 
			"Urea Nitrogen" double precision, "Hematocrit" double precision, "Hemoglobin" double precision, "MCH" double precision,
		    "MCHC" double precision, "MCV" double precision, "Platelet Count" double precision, "RDW" double precision, 
		    "Red Blood Cells" double precision, "White Blood Cells" double precision);	   
		   

-- creating diagnosis data 
drop materialized view if exists diagnosis_data cascade;
create materialized view diagnosis_data as
select * from crosstab(
	'select i.stay_id,icd_version,concat(''seq_num'', cast(di.seq_num as text)) as label, icd_code from diagnoses_icd di 
		inner join icustays i on i.hadm_id = di.hadm_id 
		where seq_num in (1,2)
		order by 1,2',
		$$Values ('seq_num1'::text), ('seq_num2')$$)
as ct(stay_id integer, icd_version integer, "Diagnosis_1" text, "Diagnosis_2" text);		   

-- final data extraction
-- with deathtime bigger than the outtime and LOS between 1 and 21
select * from (
  select i.los,i.first_careunit,a.admission_type,a.admission_location, d."Diagnosis_1", d."Diagnosis_2",d.icd_version,p.gender, p.anchor_age, cd.heart_rate, cd.respiration, cd.saturation, cd.temperature,
  cd.glc_eye , cd.glc_verbal, cd.glc_motor, ld."Anion Gap", ld."Bicarbonate", ld."Chloride", ld."Creatinine", ld."Glucose", ld."Magnesium", ld."Potassium",
  ld."Sodium", ld."Urea Nitrogen" , ld."Hematocrit", ld."Hemoglobin", ld."MCH", ld."MCHC", ld."MCV", ld."Platelet Count", ld."RDW", ld."Red Blood Cells", ld."White Blood Cells" from icustays i
    inner join admissions a on a.hadm_id = i.hadm_id 
    inner join no_readmissions nr on nr.stay_id = i.stay_id
    inner join chart_data cd on cd.stay_id = i.stay_id
    inner join lab_data ld on ld.stay_id = i.stay_id
    inner join patients p on p.subject_id = i.subject_id
    inner join diagnosis_data d on d.stay_id = i.stay_id
      where (a.deathtime - i.outtime) > '00:00:000' or a.deathtime is null 
    ) as data where los > 1 and los <= 21;
    