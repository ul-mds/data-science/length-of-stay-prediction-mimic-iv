# Prediction of Intensive Care Unit Length of Stay in the MIMIC-IV dataset

This repository is dedicated to the code of the paper "Prediction of
Intensive Care Unit Length of Stay in the MIMIC-IV dataset" DOI: ....

The code is separated into:

1.  data extraction
2.  data preparation
3.  modelling
4.  evaluation

## data extraction

The data extraction is done by a SQL query on a PostgreSQL server where
MIMIC V is stored. We are using the MIMIC V version 2.1 and for the
organisation of the PostgreSQL server we are using this repository:
<https://github.com/MIT-LCP/mimic-code> For the extraction of the data
as CSV data use the [file](./Data%20extraction/Extraction.sql).In this
script there are several commands which can be use to select the
important items and features.

## data preparation

As the second step is the data preparation for the modeling. Therefore
we are loading the data into the R and applying the defined data
cleaning rules to that data. After that we transform the data into the
two different datasets for classification and regression task.

## modelling

For the modelling to the two different tasks we decided to use the 4
algorithms: logistic regression, support vector machine, random forest
and XGBoost.

For the logistic regression we are using the inbuilt function `lm()`.
For the SVM we are using the `e1070` package and its function`svm()` for the 
prediction of the length of stay. The random forest algorithm from the package 
`randomforest` is used for there algorithm and the same goes for the XGBoost 
package and algorithm.

## evaluation

In the evaluation there is at the current state only an simple analysis of the 
prediction task. Further code we deeper analysis could be added later. The 
evaluation of the classification will also be added. 

The code for the visualization is not be placed in thhis repository but if you
would like to have further information about the graphs, we are open for
questions and discussion.
